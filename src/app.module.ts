import { HttpModule, Module, OnModuleInit } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import {
  TelegramBot,
  TelegramModule,
  TelegramModuleOptions,
  TelegramModuleOptionsFactory,
} from 'nest-telegram';
import { BalanceAction } from './action/balance-action';
import { ModuleRef } from '@nestjs/core';

@Module({
  imports: [
    HttpModule,
    TelegramModule.forRootAsync({
      useClass: class implements TelegramModuleOptionsFactory {
        createOptions(): TelegramModuleOptions {
          return {
            token: '1791586284:AAGAPhOyFDt44uBChaEPw2mU-Gg3d2CpwTQ',
            usePolling: true,
          };
        }
      },
    }),
  ],
  controllers: [AppController],
  providers: [AppService, BalanceAction],
})
export class AppModule implements OnModuleInit {
  constructor(
    private readonly bot: TelegramBot,
    private readonly ref: ModuleRef,
  ) {}

  onModuleInit() {
    this.bot.init(this.ref);
  }
}
