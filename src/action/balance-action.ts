import { HttpService, Injectable } from '@nestjs/common';
import { TelegramActionHandler, Context } from 'nest-telegram';

@Injectable()
export class BalanceAction {
  constructor(private readonly http: HttpService) {}

  @TelegramActionHandler({ onStart: true })
  public async onStart(ctx: Context): Promise<void> {
    await ctx.reply('Welcome to me');
  }

  @TelegramActionHandler({ command: '/balance' })
  public async onBalance(ctx: Context): Promise<void> {
    const balance = await this.http
      .get<{ balance: number }>('https://binance.dev-x.cloud/binance/balance')
      .toPromise();
    await ctx.reply(`Баланс этого бомжа: ${balance.data.balance.toFixed(2)}$`);
  }
}
